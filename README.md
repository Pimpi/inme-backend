[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-backend/badges/development/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-backend/-/commits/development)
[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-backend/badges/development/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-backend/-/commits/development)
[![pylint report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-backend/-/jobs/artifacts/development/raw/pylint/pylint.svg?job=pylint)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-backend/-/jobs/artifacts/development/raw/pylint/pylint.log?job=pylint)
# InMe Backend

InMe uses Django REST as backend.

## Installation

1. Create virtual environtment (e.g. venv) for this project
```
python -m venv env
```
2. Clone this project to your local
```
git clone https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2022/Kelas-B/adamy-ganteng/inme-backend.git
```
1. Install the required package
```
pip install -r requrirements.txt
``` 
3. Create your own working branch
```
git checkout -b "new_branch"
```
4. Download the .env file into the git repository
5. Now you are ready to contribute :)

## Contributing
Before commit, do static analysis of the code with
```
pylint apps
```
and code formatting with
```
black .
```
Make sure the tatic analysis score is at least above 8.0. **Always make sure your branch up-to-date with the parent branch before pushing or create merge request.** Please write a descriptive commit and merge request about what you have done so it's easier to check.