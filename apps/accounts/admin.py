from django.contrib import admin


class AccountAdmin(admin.ModelAdmin):
    list_display = ["username", "email", "date_joined", "last_login"]
