from django.contrib.auth import get_user_model
from django.test import TestCase


class AccountManagerTest(TestCase):
    def setUp(self):
        self.EMAIL = "user@example.com"

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(email=self.EMAIL, password="test")
        self.assertEqual(user.email, self.EMAIL)
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        self.assertEqual(user.__str__(), "user@example.com")
        self.assertIsNone(user.username)
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email="")
        with self.assertRaises(ValueError):
            User.objects.create_user(email="", password="foo")

    def test_create_superuser(self):
        User = get_user_model()
        admin_user = User.objects.create_superuser(email=self.EMAIL, password="foo")
        self.assertEqual(admin_user.email, self.EMAIL)
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        self.assertIsNone(admin_user.username)
        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email="super@user.com", password="foo", is_superuser=False
            )
        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email="super@user.com", password="foo", is_staff=False
            )
