from django.test import TestCase, Client


class AccountsUrls(TestCase):
    def test_register_url_is_exist_get_method(self):
        response = Client().get("/api/v1/register/")
        self.assertEqual(response.status_code, 405)

    def test_register_url_is_exist_post_method_ok(self):
        response = Client().post(
            "/api/v1/register/",
            {"email": "email@example.com", "password": "test_123_password_hello"},
        )
        self.assertEquals(response.status_code, 201)

    def test_register_url_is_exist_post_method_failed(self):
        Client().post(
            "/api/v1/register/",
            {"email": "email@example.com", "password": "test_123_password_hello"},
        )

        response = Client().post(
            "/api/v1/register/",
            {"email": "email@example.com", "password": "test_123_password_hello"},
        )
        self.assertEquals(response.status_code, 400)

        response = Client().post(
            "/api/v1/register/",
            {"email": "email2@example.com", "password": "test"},
        )
        self.assertEquals(response.status_code, 400)
