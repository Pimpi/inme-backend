"""
Create function to proceed request and return response in json for accounts
"""

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from apps.profile.models import Profile

from .serializers import AccountSerializer


@api_view(["POST"])
def register(request):
    """Proceed request to create user"""
    serializer = AccountSerializer(data=request.data)
    if serializer.is_valid():
        account = serializer.save()
        Profile.objects.create(user=account).save()
        response = {
            "status": 201,
            "email": account.email,
            "message": "Successfully created new account",
        }
        return Response(response, status=status.HTTP_201_CREATED)
    try:
        response = {"status": 400, "non_field_error": serializer.errors["email"][0]}
        return Response(response, status=status.HTTP_400_BAD_REQUEST)
    except KeyError:
        response = {
            "status": 400,
            "field_error": serializer.errors["non_field_errors"][0],
        }
        return Response(response, status=status.HTTP_400_BAD_REQUEST)
