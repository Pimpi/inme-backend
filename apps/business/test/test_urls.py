import os
from time import sleep

from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.test.client import encode_multipart, MULTIPART_CONTENT, BOUNDARY
from django.conf import settings

from rest_framework_simplejwt.tokens import RefreshToken

from ..models import Business


class BusinessUrlsTest(TestCase):
    def setUp(self):
        self.URL = "/api/v1/business/"
        self.image_file = open(
            os.path.join(
                settings.BASE_DIR, "media/images/business/default/default.png"
            ),
            "rb",
        )
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.data = {
            "business_name": "test_business",
            "description": "test_description",
            "target": 25,
            "pictures": SimpleUploadedFile(
                self.image_file.name, self.image_file.read()
            ),
            "no_rekening": "1234567890",
            "nama_bank": "BCA",
        }

    def test_business_not_authenticated(self):
        response = Client().post(path=self.URL, data=self.data)
        self.assertEqual(response.status_code, 401)

    def test_business_token_malformed(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().post(
            path=self.URL,
            data=self.data,
            HTTP_AUTHORIZATION=f"Token {refresh_token.access_token}",
        )
        self.assertEqual(response.status_code, 401)

    def test_business_token_invalid(self):
        response = Client().post(
            path=self.URL, data=self.data, HTTP_AUTHORIZATION="Bearer " + "fake_token"
        )
        self.assertEqual(response.status_code, 401)

    def test_create_business_no_picture(self):
        refresh_token = RefreshToken.for_user(self.user)
        data = {
            "business_name": "test_business",
            "description": "test_description",
            "target": 25,
            "no_rekening": "1234567890",
            "nama_bank": "BCA",
        }
        response = Client().post(
            path=self.URL,
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEqual(response.status_code, 400)

    def test_create_business(self):
        self.assertEqual(0, len(Business.objects.all()))
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().post(
            path=self.URL,
            data=self.data,
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEqual(1, len(Business.objects.all()))

    def test_update_business(self):
        refresh_token = RefreshToken.for_user(self.user)
        Client().post(
            path=self.URL,
            data=self.data,
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        business = Business.objects.all().first()
        self.assertEqual(business.business_name, "test_business")
        response = Client().put(
            path=self.URL + str(business.id) + "/",
            data=encode_multipart(BOUNDARY, {"business_name": "test_business2"}),
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
            content_type=MULTIPART_CONTENT,
        )
        business = Business.objects.all().first()
        self.assertEqual(business.business_name, "test_business2")

    def test_get_current_my_business(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().get(
            path=self.URL + "current/",
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEqual(response.status_code, 200)

    def test_get_investors(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().post(
            path=self.URL,
            data=self.data,
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        business = Business.objects.all().first()
        response = Client().get(
            path=self.URL + str(business.id) + "/investors/",
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEqual(response.status_code, 200)

    def test_get_investors_business_not_found(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().get(
            path=self.URL + "10000/investors/",
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEqual(response.status_code, 400)
