from django.contrib.auth import get_user_model
from django.test import TestCase

from apps.business.models import Business

from django.db.utils import IntegrityError
from django.utils import timezone


class BusinessTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.BUSINESS_NAME = "msglow"
        self.DESCRIPTION = "klinik ter-ok"
        self.TARGET = 200
        self.CATEGORY = "beauty"
        self.INVESTED_FUNDS = 100
        self.NO_NPWP = "123456"
        self.NO_REKENING = "1234567890"
        self.NAMA_BANK = "BCA"

    def test_create_business(self):
        business = Business.objects.create(
            user_id=self.user,
            business_name=self.BUSINESS_NAME,
            description=self.DESCRIPTION,
            target=self.TARGET,
            category=self.CATEGORY,
            invested_funds=self.INVESTED_FUNDS,
            no_npwp=self.NO_NPWP,
            no_rekening=self.NO_REKENING,
            nama_bank=self.NAMA_BANK,
        )

        self.assertEqual(business.__str__(), self.BUSINESS_NAME)
        self.assertEqual(business.business_name, self.BUSINESS_NAME)
        self.assertEqual(business.description, self.DESCRIPTION)
        self.assertEqual(business.target, self.TARGET)
        self.assertEqual(business.category, self.CATEGORY)
        self.assertEqual(business.invested_funds, self.INVESTED_FUNDS)
        self.assertEqual(business.no_npwp, self.NO_NPWP)
        self.assertEqual(business.nama_bank, self.NAMA_BANK)
        self.assertEqual(business.no_rekening, self.NO_REKENING)
        self.assertNotEqual(business.__str__(), "tongfang")
        self.assertNotEqual(business.business_name, "tongfang")
        self.assertNotEqual(business.description, "kurang ini kliniknya")
        self.assertNotEqual(business.category, "jelek")
        self.assertNotEqual(business.no_npwp, "2108223")
        with self.assertRaises(IntegrityError):
            Business.objects.create()
