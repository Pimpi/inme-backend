from django.contrib import admin

from .models import Business


class BusinessAdmin(admin.ModelAdmin):
    list_display = ["user", "business_name", "description"]
