"""
Create function to proceed request and return response in json for business
"""

from rest_framework import serializers, viewsets, status
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response

from django.contrib.auth import get_user_model

from apps.investors.models import Investor
from apps.investors.serializers import InvestorSerializer
from apps.pictures.serializers import PictureSerializer
from apps.pictures.models import Picture
from apps.business.models import Business


class BusinessSerializer(serializers.ModelSerializer):
    """Serializers for business"""

    user_id = serializers.PrimaryKeyRelatedField(
        default=serializers.CurrentUserDefault(),
        queryset=get_user_model().objects.all()
    )
    pictures = PictureSerializer(many=True, read_only=True)

    class Meta:
        model = Business
        read_only_fields = ["date_created"]
        fields = [
            "user_id",
            "id",
            "business_name",
            "description",
            "target",
            "category",
            "no_npwp",
            "nama_bank",
            "no_rekening",
            "date_created",
            "invested_funds",
            "pictures",
        ]


class BusinessViewSet(viewsets.ModelViewSet):
    """Endpoints for business all methods"""

    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer

    @action(detail=True)
    def investors(self, request, pk=None):
        """Get all business investments"""
        try:
            Business.objects.get(id=pk)
            investors = Investor.objects.prefetch_related("user__profile").filter(
                business_id=pk
            )
            investor_serializer = InvestorSerializer(investors, many=True)
            formatted_serializer = [
                {
                    "name": investor["user"]["profile"]["nama"],
                    "picture": investor["user"]["profile"]["picture"],
                    "amount": investor["amount"],
                }
                for investor in investor_serializer.data
            ]
            return Response(formatted_serializer)
        except Business.DoesNotExist:
            return Response(
                {"message": "No business found with that pk"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    @action(detail=False, permission_classes=[IsAuthenticated])
    def current(self, request):
        """Endpoint for my business"""
        current_user = request.user
        my_business = Business.objects.filter(user_id=current_user)
        serializer = self.get_serializer(my_business, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """Override endpoint create business"""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            pictures = dict((request.data).lists())["pictures"]
        except (KeyError, AttributeError):
            return Response(
                {"non_field_erros": ["The fields pictures cannot be empty"]},
                status=status.HTTP_400_BAD_REQUEST,
            )
        business = serializer.save()
        for picture in pictures:
            data = {"business": business.id, "picture": picture}
            serializer2 = PictureSerializer(data=data)
            serializer2.is_valid(raise_exception=True)
            serializer2.save()
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        """Override endpoint create business"""
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        try:
            pictures = dict((request.data).lists())["pictures"]
            if pictures:
                for picture in Picture.objects.filter(business=instance.id):
                    picture.delete()
                for picture in pictures:
                    data = {"business": instance.id, "picture": picture}
                    serializer2 = PictureSerializer(data=data)
                    serializer2.is_valid(raise_exception=True)
                    serializer2.save()
        except (KeyError, AttributeError):
            pass

        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, "_prefetched_objects_cache", None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
