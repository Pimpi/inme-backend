from django.contrib.auth import get_user_model
from django.db import models


class Business(models.Model):
    user_id = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    business_name = models.CharField(max_length=255)
    description = models.CharField(
        max_length=255,
    )
    target = models.DecimalField(max_digits=11, decimal_places=2)
    category = models.CharField(max_length=255, default="null")
    date_created = models.DateTimeField(auto_now_add=True)
    invested_funds = models.DecimalField(max_digits=11, decimal_places=2, default=0.0)
    no_npwp = models.CharField(max_length=255, default="null")
    nama_bank = models.CharField(max_length=255)
    no_rekening = models.CharField(max_length=255)

    class Meta:
        unique_together = ["user_id", "business_name"]

    def __str__(self):
        return self.business_name
