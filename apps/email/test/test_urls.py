import os
from time import sleep

from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.test.client import encode_multipart, MULTIPART_CONTENT, BOUNDARY
from django.conf import settings

from rest_framework_simplejwt.tokens import RefreshToken


class EmailUrlsTest(TestCase):
    def setUp(self):
        self.URL = "/api/v1/email/"
        self.user1 = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.user2 = get_user_model().objects.create(
            email="user2@example.com", password="test1234567"
        )
        self.data = {
            "sender": self.user1.email,
            "receiver": self.user2.id,
            "message": "hallo",
        }

    def test_email_not_authenticated(self):
        response = Client().post(path=self.URL, data=self.data)
        self.assertEqual(response.status_code, 401)

    def test_create_email(self):
        refresh_token = RefreshToken.for_user(self.user1)
        response = Client().post(
            path=self.URL,
            data=self.data,
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEqual(response.status_code, 201)