from django.urls import path, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()
router.register("", EmailViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
