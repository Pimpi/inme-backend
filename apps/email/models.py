from django.contrib.auth import get_user_model
from django.db import models


class Email(models.Model):
    sender = models.ForeignKey(
        get_user_model(), related_name="sent_messages", on_delete=models.CASCADE
    )
    receiver = models.ForeignKey(
        get_user_model(), related_name="received_messages", on_delete=models.CASCADE
    )
    message = models.TextField(blank=False, null=False)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"[{self.sender} - {self.receiver}]"
