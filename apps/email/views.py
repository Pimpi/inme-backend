"""
Create function to proceed request and return response in json for business
"""
from django.contrib.auth import get_user_model
from rest_framework import serializers, viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.profile.models import Profile
from apps.accounts.models import CustomUser

from .models import Email

User = get_user_model()

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ["nama", "picture"]
class UserProfileSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()
    class Meta:
        model = User
        fields = ["profile", "email", "id"]

class EmailSerializer(serializers.ModelSerializer):
    """Serializers for business"""
    sender = UserProfileSerializer()
    receiver = UserProfileSerializer()
    class Meta:
        model = Email
        read_only_fields = ["created"]
        fields = [
            "sender",
            "receiver",
            "message",
            "created"
        ]


class EmailViewSet(viewsets.ModelViewSet):
    """Endpoints for email all methods"""

    permission_classes = [IsAuthenticated]
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
    
    @action(detail=False)
    def current(self, request):
        """Endpoint for my business"""
        current_user = request.user
        email_received = Email.objects.filter(receiver=current_user)
        email_sent = Email.objects.filter(sender=current_user)
        serializer_received = self.get_serializer(email_received, many=True)
        serializer_sent = self.get_serializer(email_sent, many=True)
        return Response(serializer_received.data + serializer_sent.data)   

    def create(self, request, *args, **kwargs):
        """Override endpoint create business"""
        try:
            sender = CustomUser.objects.get(email=request.data['sender'])
            receiver = CustomUser.objects.get(id=request.data['receiver'])
        except CustomUser.DoesNotExist:
            return Response({"message":"user not found"}, status=status.HTTP_400_BAD_REQUEST)
        email = Email.objects.create(sender=sender, receiver=receiver, message=request.data['message'])
        email.save()
        serializer = self.get_serializer(email)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED
        )

