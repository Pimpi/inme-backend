from django.contrib import admin


class EmailAdmin(admin.ModelAdmin):
    list_display = ["sender", "receiver"]
