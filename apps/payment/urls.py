from django.urls import path

from apps.payment.views import charge, handle_notification, get_status


urlpatterns = [
    path("charge/", charge),
    path("handle-notification/", handle_notification),
    path("status/<uuid:order_id>/", get_status),
]
