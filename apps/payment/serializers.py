from rest_framework import serializers

from apps.payment.models import InvestorPayment


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvestorPayment
        fields = "__all__"
