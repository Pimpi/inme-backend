import os
import uuid
from zoneinfo import ZoneInfo
from datetime import datetime, timedelta
from midtransclient import CoreApi, MidtransAPIError
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from apps.business.models import Business

from apps.payment.models import InvestorPayment, Status
from apps.investors.models import Investor

from .serializers import PaymentSerializer

midtrans_core_api = CoreApi(
    is_production=False,
    server_key=os.environ.get("MIDTRANS_SERVER_KEY"),
    client_key=os.environ.get("MIDTRANS_CLIENT_KEY"),
)


def get_midtrans_request_from_body(body):
    """
    Make midtrans charge request from network request body
    """
    data = {}
    if body["method"] == "permata":
        data["payment_type"] = "permata"
    elif body["method"] in ["bca", "bni", "bri"]:
        data["payment_type"] = "bank_transfer"
        data["bank_transfer"] = {"bank": body["method"]}

    gross_amount = body["invest_amount"] + body["fee"]
    data["transaction_details"] = {
        "gross_amount": gross_amount,
        "order_id": str(uuid.uuid4()),
    }

    data["metadata"] = {"fee": f'{body["fee"]:.2f}'}

    return data


def get_expired_time_from_transaction_time(formatted_time):
    """
    Create iso formatted expired time from transaction time
    """
    time = datetime.strptime(formatted_time, "%Y-%m-%d %H:%M:%S").replace(
        tzinfo=ZoneInfo("Asia/Jakarta")
    )
    expired_time = time + timedelta(hours=1)
    return expired_time.isoformat()


def get_charge_response_from_payment(payment_object, fee):
    """
    Make charge response from payment serializer validated data
    """
    charge_response = {}

    charge_response["payment_id"] = str(payment_object.id)

    charge_response["investor_id"] = payment_object.investor_id

    charge_response["invested_business_id"] = payment_object.invested_business_id

    charge_response["status"] = payment_object.status

    fee = float(fee)
    gross_amount = float(payment_object.midtrans_response["gross_amount"])
    charge_response["amount"] = f"{gross_amount-fee:.2f}"
    charge_response["fee"] = f"{fee:.2f}"

    payment_type = payment_object.midtrans_response["payment_type"]
    is_type_permata = "permata_va_number" in payment_object.midtrans_response
    if is_type_permata:
        permata_va_number = payment_object.midtrans_response["permata_va_number"]
        charge_response["va_numbers"] = [
            {"bank": "permata", "va_number": permata_va_number}
        ]
    elif payment_type == "bank_transfer":
        va_numbers = payment_object.midtrans_response["va_numbers"]
        charge_response["va_numbers"] = va_numbers

    transaction_time = payment_object.midtrans_response["transaction_time"]
    expired_time = get_expired_time_from_transaction_time(transaction_time)
    charge_response["expired_time"] = expired_time

    return charge_response


@api_view(["POST"])
@permission_classes([IsAuthenticated])
def charge(request):
    """
    Charge payment request to midtrans core-api
    """
    business_id = request.data["business_id"]

    midtrans_response = {}
    try:
        midtrans_request = get_midtrans_request_from_body(request.data)
        midtrans_response = midtrans_core_api.charge(midtrans_request)
    except MidtransAPIError as midtrans_error:
        midtrans_response = midtrans_error.api_response_dict
        return Response(
            {"message": "Error from Midtrans."},
            status=status.HTTP_500_INTERNAL_SERVER_ERROR,
        )

    # charge midtrans successfull
    payment_data = {
        "id": midtrans_response["order_id"],
        "investor": request.user.id,
        "invested_business": business_id,
        "midtrans_response": midtrans_response,
    }
    payment_serializer = PaymentSerializer(data=payment_data)
    if payment_serializer.is_valid():
        payment_object = payment_serializer.save()
        fee = midtrans_request["metadata"]["fee"]
        charge_response = get_charge_response_from_payment(payment_object, fee)
        return Response(charge_response, status=status.HTTP_200_OK)
    return Response(
        {"message": "An error has occurred."},
        status=status.HTTP_500_INTERNAL_SERVER_ERROR,
    )


def create_or_update_investor(investor_payment):
    """
    Create investor model if the first time investing, add previous investment
    with a new investment amount
    """
    gross_amount = float(investor_payment.midtrans_response["gross_amount"])
    fee = float(investor_payment.midtrans_response["metadata"]["fee"])
    amount = gross_amount - fee

    investor = None
    try:
        business = Business.objects.get(id=investor_payment.invested_business_id)
        business.invested_funds = float(business.invested_funds) + amount
        business.save()

        investor = Investor.objects.get(
            user=investor_payment.investor_id,
            business=investor_payment.invested_business_id,
        )
        investor.amount = float(investor.amount) + amount
    except Investor.DoesNotExist:
        investor = Investor(
            business=investor_payment.invested_business,
            user=investor_payment.investor,
            amount=amount,
        )
    investor.save()


@api_view(["POST"])
def handle_notification(request):
    """
    Handle payment notification from Midtrans
    """
    midtrans_validated_notification = midtrans_core_api.transactions.notification(
        request.data
    )

    order_uuid = uuid.UUID(midtrans_validated_notification["order_id"])
    transaction_status = midtrans_validated_notification["transaction_status"]

    investor_payment = None
    try:
        investor_payment = InvestorPayment.objects.get(id=order_uuid)
    except InvestorPayment.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if transaction_status == "settlement" and investor_payment.status == Status.PENDING:
        investor_payment.status = Status.SUCCESS
        investor_payment.midtrans_response = midtrans_validated_notification
        create_or_update_investor(investor_payment)
        investor_payment.save()
    elif transaction_status in ["cancel", "deny", "expire"]:
        investor_payment.status = Status.FAILURE
        investor_payment.midtrans_response = midtrans_validated_notification
        investor_payment.save()

    return Response(status=status.HTTP_200_OK)


@api_view(["GET"])
def get_status(request, order_id):
    """
    Get status by order_id
    """
    investor_payment = None
    try:
        investor_payment = InvestorPayment.objects.get(id=order_id)
    except InvestorPayment.DoesNotExist:
        return Response(
            {"message": "No payment found with that uuid"},
            status=status.HTTP_404_NOT_FOUND,
        )

    charge_response = get_charge_response_from_payment(investor_payment, "50000")
    return Response(charge_response, status=status.HTTP_200_OK)
