from uuid import uuid4
from django.contrib.auth import get_user_model
from django.db import models

from apps.business.models import Business


class Status(models.TextChoices):
    PENDING = "pending"
    FAILURE = "failure"
    SUCCESS = "success"


class InvestorPayment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=True)
    investor = models.ForeignKey(
        get_user_model(), related_name="payments", on_delete=models.CASCADE
    )
    invested_business = models.ForeignKey(
        Business, related_name="payments", on_delete=models.CASCADE
    )
    status = models.CharField(
        max_length=7, choices=Status.choices, default=Status.PENDING
    )
    midtrans_response = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
