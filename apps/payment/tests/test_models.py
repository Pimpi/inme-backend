from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import InvestorPayment, Status
from apps.business.models import Business


class InvestorTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.business = Business.objects.create(
            user_id=self.user,
            business_name="test_name",
            description="test_desc",
            target=15.0,
        )

    def test_status_return_correct_string(self):
        self.assertEqual(Status.PENDING, "pending")
        self.assertEqual(Status.FAILURE, "failure")
        self.assertEqual(Status.SUCCESS, "success")

    def test_create_investorpayment_success(self):
        self.assertEqual(len(InvestorPayment.objects.all()), 0)
        investor_payment = InvestorPayment.objects.create(
            investor=self.user,
            invested_business=self.business,
            midtrans_response={},
        )
        self.assertEqual(len(InvestorPayment.objects.all()), 1)
        self.assertEqual(investor_payment.investor, self.user)
        self.assertEqual(investor_payment.invested_business, self.business)
        self.assertEqual(investor_payment.midtrans_response, {})
