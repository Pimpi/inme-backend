from django.contrib.auth import get_user_model
from django.test import TestCase, Client

from rest_framework_simplejwt.tokens import RefreshToken


class PaymentUrlsTest(TestCase):
    def setUp(self):
        self.URL = "/api/v1/payment/"
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )

    def test_payment_charge_not_authenticated(self):
        response = Client().post(path=self.URL + "charge/")
        self.assertEqual(response.status_code, 401)

    def test_payment_handle_notification_wrong_method(self):
        response = Client().get(path=self.URL + "handle-notification/")
        self.assertEqual(response.status_code, 405)

    def test_payment_status_without_url_parameter(self):
        response = Client().get(path=self.URL + "status/")
        self.assertEqual(response.status_code, 404)
