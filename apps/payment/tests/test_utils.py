import uuid

from django.test import TestCase

from ..views import get_midtrans_request_from_body


class PaymentUtilsTest(TestCase):
    def test_get_midtrans_request_from_body(self):
        payload = {
            "method": "permata",
            "invest_amount": 500000,
            "fee": 50000,
        }

        data = get_midtrans_request_from_body(body=payload)
        self.assertEqual(data["payment_type"], "permata")
        self.assertEqual(data["transaction_details"]["gross_amount"], 550000)
        order_id_check = (
            True if uuid.UUID(data["transaction_details"]["order_id"]) else False
        )
        self.assertEqual(order_id_check, True)
