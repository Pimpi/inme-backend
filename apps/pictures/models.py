from django.db import models
from django.conf import settings

from apps.business.models import Business


def upload_path(instance, filename):
    return "/".join(["images", "business", str(instance.business.id), filename])


class Picture(models.Model):
    business = models.ForeignKey(
        Business, related_name="pictures", on_delete=models.CASCADE
    )
    picture = models.ImageField(
        default="images/business/default/default.png",
        upload_to=upload_path,
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = ["business", "picture"]

    def __str__(self):
        return self.picture.url
