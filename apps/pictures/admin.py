from django.contrib import admin


class PictureAdmin(admin.ModelAdmin):
    list_display = ["business", "picture"]
