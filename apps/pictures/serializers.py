"""
Provide converter to python datatypes and json for picture model
"""

from rest_framework import serializers
from .models import Picture


class PictureSerializer(serializers.ModelSerializer):
    """Serializer for picture"""

    class Meta:
        model = Picture
        fields = ["business", "picture"]
