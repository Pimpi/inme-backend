from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import Picture, upload_path
from apps.business.models import Business


class PictureTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.business = Business.objects.create(
            user_id=self.user,
            business_name="test_name",
            description="test_desc",
            target=15.0,
        )

    def test_create_picture(self):
        self.assertEqual(0, len(Picture.objects.all()))
        picture = Picture.objects.create(business=self.business, picture="test/1")
        self.assertEqual(1, len(Picture.objects.all()))
        self.assertEqual("/media/test/1", str(picture))

    def test_upload_path(self):
        picture = Picture.objects.create(business=self.business, picture="test/1")
        path = upload_path(instance=picture, filename="dummy_image.jpg")
        self.assertEqual("images/business/1/dummy_image.jpg", path)
