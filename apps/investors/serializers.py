from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.pictures.serializers import PictureSerializer
from apps.investors.models import Investor
from apps.business.models import Business
from apps.profile.models import Profile

User = get_user_model()

class BusinessNamePictureSerializer(serializers.ModelSerializer):
    pictures = PictureSerializer(many=True)
    class Meta:
        model = Business
        fields = ['business_name', 'pictures']

class InvestorBusinessSerializer(serializers.ModelSerializer):
    business = BusinessNamePictureSerializer()
    class Meta:
        model = Investor
        fields = ['business', 'amount']

class ProfileNamePictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ["nama", "picture"]


class UserProfileSerializer(serializers.ModelSerializer):
    profile = ProfileNamePictureSerializer()

    class Meta:
        model = User
        fields = ["profile"]


class InvestorSerializer(serializers.ModelSerializer):
    user = UserProfileSerializer()

    class Meta:
        model = Investor
        fields = ["user", "amount"]
