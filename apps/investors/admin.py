from django.contrib import admin


class InvestorAdmin(admin.ModelAdmin):
    list_display = ["business", "user", "amount", "updated"]
