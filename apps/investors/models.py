from django.contrib.auth import get_user_model
from django.db import models

from apps.business.models import Business


class Investor(models.Model):
    business = models.ForeignKey(
        Business, related_name="investors", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        get_user_model(), related_name="investments", on_delete=models.CASCADE
    )
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ["business", "user"]

    def __str__(self):
        return f"[{self.business} - {self.user}]"
