from django.test import TestCase

from ..views import format_investor

class InvestorsUtilsTest(TestCase):
    def test_format_investor_picture_available(self):
        data = {
            "amount": 500000,
            "business": {
                "business_name": "test123",
                "pictures": [{"picture":"halo"}]
            }
        }
        output = format_investor(data)
        self.assertEqual(output['amount'], 500000)
        self.assertEqual(output['business_name'], "test123")
        self.assertEqual(output['picture'], "halo")