from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import Investor
from apps.business.models import Business


class InvestorTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.business = Business.objects.create(
            user_id=self.user,
            business_name="test_name",
            description="test_desc",
            target=15.0,
        )

    def test_create_investor(self):
        self.assertEqual(0, len(Investor.objects.all()))
        investor = Investor.objects.create(
            business=self.business, user=self.user, amount=100.00
        )
        self.assertEqual(1, len(Investor.objects.all()))
        self.assertEqual(f"[{self.business} - {self.user}]", str(investor))
