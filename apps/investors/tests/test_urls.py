from django.contrib.auth import get_user_model
from django.test import TestCase, Client

from rest_framework_simplejwt.tokens import RefreshToken


class InvestorsUrlsTest(TestCase):
    def setUp(self):
        self.URL = "/api/v1/investment/"
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )

    def test_investment_not_authenticated(self):
        response = Client().get(path=self.URL + "myinvestment/")
        self.assertEqual(response.status_code, 401)

    def test_investment_success(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().get(
            path=self.URL + "myinvestment/",
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEqual(response.status_code, 200)
