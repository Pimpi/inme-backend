from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from apps.investors.serializers import InvestorBusinessSerializer

from .models import Investor

def format_investor(investor):
    output = {
        'amount': investor['amount'],
        'business_name': investor['business']['business_name'],
    }
    if len(investor['business']['pictures']) > 0:
        output['picture'] = investor['business']['pictures'][0]['picture']
    return output

@api_view(["GET"])
@permission_classes([IsAuthenticated])
def my_investment(request):
    """
    Get user invested business
    """
    user = request.user
    investments = Investor.objects.filter(user=user)
    serializer = InvestorBusinessSerializer(investments, many=True)
    serializer_data = serializer.data

    formatted_serializer = [
        format_investor(investor) for investor in serializer_data
    ]

    return Response(formatted_serializer)
