"""
Create function to proceed request and return response in json for profile
"""

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import Profile
from .serializers import ProfileSerializer


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get(request):
    """
    Return user profile if token valid
    """
    user = request.user
    profile = Profile.objects.get(user=user)
    response = {
        "email": user.email,
        "name": profile.nama,
        "no_telp": profile.no_telp,
        "no_ktp": profile.no_ktp,
        "no_npwp": profile.no_npwp,
        "picture": profile.picture.url,
    }
    return Response(response, status=status.HTTP_200_OK)


@api_view(["PUT"])
@permission_classes([IsAuthenticated])
def edit(request):
    """
    Update user profile
    """
    user = request.user
    profile = Profile.objects.get(user=user)
    serializer = ProfileSerializer(profile, data=request.data, partial=True)
    if serializer.is_valid():
        profile = serializer.save()
    response = {
        "status": 200,
        "profile": ProfileSerializer(profile).data,
        "message": "Successfully edit profile",
    }
    return Response(response, status=status.HTTP_200_OK)
