from time import sleep
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.test.client import encode_multipart, MULTIPART_CONTENT, BOUNDARY

from rest_framework_simplejwt.tokens import RefreshToken

from ..models import Profile


class ProfileUrlTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.NAMA = "user_example"
        self.NO_TELP = "7261378123"
        self.NO_KTP = "328149712"
        self.NO_NPWP = "128371298312"
        self.profile = Profile.objects.create(
            user=self.user,
            nama=self.NAMA,
            no_telp=self.NO_TELP,
            no_ktp=self.NO_KTP,
            no_npwp=self.NO_NPWP,
        )
        self.RESOURCE_URL = "/api/v1/accounts/resource/"

    def test_edit_resource_but_get_method(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().get(
            self.RESOURCE_URL + "edit/",
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
        )
        self.assertEquals(response.status_code, 405)

    def test_edit_resource_but_no_token_provided(self):
        response = Client().put(self.RESOURCE_URL + "edit/", {"nama": "user_example2"})
        self.assertEquals(response.status_code, 401)

    def test_edit_resource_but_token_not_valid(self):
        response = Client().put(
            self.RESOURCE_URL + "edit/",
            data={"nama": "user_example2"},
            HTTP_AUTHORIZATION="Bearer fake_token",
        )
        self.assertEquals(response.status_code, 401)

    def test_edit_resource_ok(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().put(
            self.RESOURCE_URL + "edit/",
            data=encode_multipart(BOUNDARY, {"nama": "user_example2"}),
            HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}",
            content_type=MULTIPART_CONTENT,
        )
        self.assertEquals(response.status_code, 200)

    def test_get_resource_invalid_token(self):
        response = Client().get(
            self.RESOURCE_URL, HTTP_AUTHORIZATION=f"Bearer fake_token"
        )
        self.assertEquals(response.status_code, 401)

    def test_get_resource_user_ok(self):
        refresh_token = RefreshToken.for_user(self.user)
        response = Client().get(
            self.RESOURCE_URL, HTTP_AUTHORIZATION=f"Bearer {refresh_token.access_token}"
        )
        self.assertEqual(response.status_code, 200)
