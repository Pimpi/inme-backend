from django.test import TestCase
from django.contrib.auth import get_user_model

from ..serializers import ProfileSerializer
from ..models import Profile


class ProfileSerializerTest(TestCase):
    def setUp(self):
        self.data = {
            "user": {"email": "user2@example.com", "password": "test1234567"},
            "nama": "user_example2",
        }

    def test_create_profile_via_serializer(self):
        serializer = ProfileSerializer(data=self.data)
        serializer.is_valid()
        data = serializer.validated_data
        self.assertDictContainsSubset(self.data, data)
        serializer.save()
        self.assertEqual(1, Profile.objects.all().count())

    def test_update_profile_via_serializer(self):
        serializer = ProfileSerializer(data=self.data)
        serializer.is_valid()
        serializer.save()
        user = get_user_model().objects.get(email="user2@example.com")
        profile = Profile.objects.get(user=user.id)
        self.assertEquals(profile.nama, "user_example2")

        new_data = {"nama": "user_example3"}
        serializer = ProfileSerializer(profile, data=new_data, partial=True)
        serializer.is_valid()
        profile = serializer.save()
        self.assertEquals(profile.nama, "user_example3")
