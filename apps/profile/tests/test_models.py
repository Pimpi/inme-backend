from django.contrib.auth import get_user_model
from django.test import TestCase

from ..models import Profile, upload_path

from django.db.utils import IntegrityError


class ProfileTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create(
            email="user@example.com", password="test1234567"
        )
        self.NAMA = "user_example"
        self.NO_TELP = "7261378123"
        self.NO_KTP = "328149712"
        self.NO_NPWP = "128371298312"

    def test_create_profile(self):
        profile = Profile.objects.create(
            user=self.user,
            nama=self.NAMA,
            no_telp=self.NO_TELP,
            no_ktp=self.NO_KTP,
            no_npwp=self.NO_NPWP,
        )

        self.assertEqual(profile.__str__(), self.NAMA)
        self.assertEqual(profile.nama, self.NAMA)
        self.assertEqual(profile.no_telp, self.NO_TELP)
        self.assertEqual(profile.no_ktp, self.NO_KTP)
        self.assertEqual(profile.no_npwp, self.NO_NPWP)
        with self.assertRaises(IntegrityError):
            Profile.objects.create()

    def test_upload_path(self):
        profile = Profile.objects.create(
            user=self.user,
            nama=self.NAMA,
            no_telp=self.NO_TELP,
            no_ktp=self.NO_KTP,
            no_npwp=self.NO_NPWP,
        )
        profile.save()
        path = upload_path(instance=self.user, filename="dummy_image.jpg")
        self.assertEqual(f"images/profile/{self.user.id}/dummy_image.jpg", path)
