from django.contrib.auth import get_user_model
from django.db import models


def upload_path(instance, filename):
    return "/".join(["images", "profile", str(instance.id), filename])


class Profile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    nama = models.CharField(max_length=255, null=True, blank=True)
    no_telp = models.CharField(max_length=255, null=True, blank=True)
    no_ktp = models.CharField(max_length=255, null=True, blank=True)
    no_npwp = models.CharField(max_length=255, null=True, blank=True)
    picture = models.ImageField(
        default="images/profile/default/default.png",
        upload_to=upload_path,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.nama
