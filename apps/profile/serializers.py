"""
Provide converter to python datatypes and json for profile model
"""

from rest_framework import serializers

from apps.accounts.models import CustomUser
from apps.accounts.serializers import AccountSerializer

from .models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    """Serialzer for create and update profile"""

    user = AccountSerializer()

    class Meta:
        model = Profile
        depth = 1
        fields = ("user", "nama", "no_telp", "no_ktp", "no_npwp", "picture")
        read_only_fields = ("user",)

    def create(self, validated_data):
        """
        Create and return a new profile instance, given the validated data
        """
        user_data = validated_data.pop("user")
        user = CustomUser.objects.create(**user_data)
        return Profile.objects.create(user=user, **validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing profile, given the validated data
        """
        instance.nama = validated_data.get("nama", instance.nama)
        instance.no_telp = validated_data.get("no_telp", instance.no_telp)
        instance.no_ktp = validated_data.get("no_ktp", instance.no_ktp)
        instance.no_npwp = validated_data.get("no_npwp", instance.no_npwp)
        instance.picture = validated_data.get("picture", instance.picture)
        instance.save()
        return instance
