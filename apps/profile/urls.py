from django.urls import path

from .views import *

urlpatterns = [
    path("resource/", get),
    path("resource/edit/", edit),
]
