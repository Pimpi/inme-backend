"""
Provides routing path for every apps
"""

from django.urls import path, include

urlpatterns = [
    path("", include("apps.accounts.urls")),
    path("accounts/", include("apps.profile.urls")),
    path("business/", include("apps.business.urls")),
    path("payment/", include("apps.payment.urls")),
    path("investment/", include("apps.investors.urls")),
    path("email/", include("apps.email.urls"))
]
