# syntax=docker/dockerfile:1
FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PORT 8080
ENV HOST 0.0.0.0

ENV PROD=true

WORKDIR /code
COPY . .

COPY ./env.txt ./.env

# install requirements
RUN pip install -r requirements.txt
# migrations
RUN python manage.py makemigrations accounts business email investors payment pictures profile
RUN python manage.py migrate
# static
RUN python manage.py collectstatic --noinput

CMD exec gunicorn --bind 0.0.0.0:$PORT --workers 1 --threads 8 --timeout 0 inme_backend.wsgi:application
